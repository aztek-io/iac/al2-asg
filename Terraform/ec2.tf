resource "aws_launch_configuration" "test_server" {
    name_prefix                     = local.asg_name
    image_id                        = data.aws_ami.al2.id
    instance_type                   = var.ec2["size"]
    key_name                        = var.ec2["key_name"]
    security_groups                 = concat(
        data.aws_security_groups.trusted.ids,
        data.aws_security_groups.default.ids
    )
    associate_public_ip_address     = true
    user_data                       = file("${path.module}/bootstrap.sh")
    spot_price                      = var.ec2["spot_price"]
    lifecycle {
        create_before_destroy = true
    }
}

resource "aws_autoscaling_group" "test_server" {
    availability_zones          = data.aws_availability_zones.available.names
    name                        = local.asg_name
    max_size                    = var.ec2["max_size"]
    min_size                    = var.ec2["min_size"]
    launch_configuration        = aws_launch_configuration.test_server.name
    vpc_zone_identifier         = data.aws_subnet_ids.public.ids
    health_check_grace_period   = 0
    tags                        = var.asg_tags
}
