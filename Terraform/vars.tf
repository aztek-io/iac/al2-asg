########################################
### Variables ##########################
########################################

variable "global" {
    type = "map"
    default = {
        region      = "us-west-2"
        project     = "aztek"
    }
}

variable "environment" {}

locals {
    asg_name = join(
        "-",
        [
            var.global["project"],
            var.environment,
            "al2"
        ]
    )
}

variable "ec2" {
    type = "map"
    default = {
        size        = "m3.medium"
        key_name    = "aztek"
        spot_price  = 0.067
        min_size    = 1
        max_size    = 3
    }
}

variable "asg_tags" {
    type    = list(map(string))
    default = [
        {
            key                 = "AutoCleanup"
            value               = true
            propagate_at_launch = true
        },
        {
            key                 = "Consulting"
            value               = true
            propagate_at_launch = true
        }
    ]
}
