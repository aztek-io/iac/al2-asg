terraform {
    backend "s3" {
        bucket  = "aztek.terraform.tfstate"
        key     = "al2/terraform.tfstate"
        region  = "us-west-2"
        encrypt = "true"
    }
}
