#!/usr/bin/env bash

invoke_main(){
    install_prereqs
    configure_powerline
}

install_prereqs(){
    yum update -y
    yum install -y python3-pip git
    pip3 install powerline-status
}

configure_powerline(){
    cat >> "$HOME/.bash_profile" << 'EOF'
# Powerline
powerline-daemon -q
export POWERLINE_BASH_CONTINUATION=1
export POWERLINE_BASH_SELECT=1

# shellcheck disable=SC1091
source "/usr/local/lib/python3.7/site-packages/powerline/bindings/bash/powerline.sh"
EOF

    cp -a "$HOME/.bash_profile" /home/ec2-user/.bash_profile
    chown "ec2-user:ec2-user" /home/ec2-user/.bash_profile
}
