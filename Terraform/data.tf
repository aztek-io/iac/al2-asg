data "aws_availability_zones" "available" {}

data "aws_ami" "al2" {
    owners      = ["amazon"]
    most_recent = true
    name_regex  = "amzn2-ami-hvm-2.0.*x86_64-gp2"
}

data "aws_security_groups" "trusted" {
    tags = {
        Name = join("-", [var.global["project"], var.environment, "ssh-trusted"])
    }
}

data "aws_security_groups" "default" {
    tags = {
        Name = join("-", [var.global["project"], var.environment, "defualt"])
    }
}

data "aws_vpc" "aztek" {
    filter {
        name = "tag:Name"
        values = [
            join("-", [var.global["project"], var.environment])
        ]
    }
}

data "aws_subnet_ids" "public" {
    vpc_id = data.aws_vpc.aztek.id

    tags = {
        Public = true
    }
}
